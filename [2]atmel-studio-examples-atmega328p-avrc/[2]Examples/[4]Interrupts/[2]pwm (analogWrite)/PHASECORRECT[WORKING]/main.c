/*
 * lm1085.c
 *
 * Created: 9/21/2020 6:14:55 AM
 * Author : Christianidis Vasileios
 */ 
#define F_CPU 16000000UL //the clock of the cpu (internal is 8 000 000 hz)
#define BRC ((F_CPU/16/9600)-1) //define the baud rate (usually 9600)

#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>
#include <util/delay.h>


//TX functions
void sw(char c[]); //print a string on serial
void swn(int num, int type, int ln); //print a Register or a number (no float), in hex(2) or in decimal(10)
void swf(double numf, uint8_t lnf, uint8_t extra_accur, uint8_t print_it); //Take a float and print it on serial

//RX functions
void readUntill(char c);

//======================TX START======================
volatile uint8_t len, k=0; //unsigned 8 bit integer
volatile char str[40], str_floatx[40];
//======================TX END=======================

//======================RX START=======================
char rxBuffer[128];
volatile char udr0 = '\0', rx_stop='\0'; /*  NULL = \0  */
volatile uint8_t rxReadPos = 0, rxWritePos = 0, readString =0;
//======================RX END=======================


//functions
void main_function(); //initializes the mcu, the first code that runs on main
void init_rxtx_function(); //initializes only rxtx functionality
void init_function(); //this goes in while(1) as a loop, repeats itself

//VARS
uint8_t inter=0;

int main(void)
{
	init_rxtx_function();
	init_function();

    while (1) 
    {
	
		main_function();
		
		_delay_ms(1000);
    }
}



void init_rxtx_function()
{
	//==================================TX START====================================
	UBRR0H = (BRC >> 8); //Put BRC to UBRR0H and move it right 8 bits.
	UBRR0L = BRC;
	UCSR0B = (1 << TXEN0); //Trans enable
	UCSR0C = (1 << UCSZ01) | (1 << UCSZ00); //8 BIT data frame

	//ENABLE interrupts
	sei();
	//==================================TX END=====================================
	//================================== RX START===========================================
	UCSR0B |= (1 << RXEN0) | (1 << RXCIE0); //RX enable and Receive complete interrupt enable
	//================================== RX END=============================================
}

void init_function()//initializes the mcu, the first code that runs on main
{			
	sw("start fast pwm\n\r");
	
	sei();
	
	//1024 PRESCALER
	TCCR0B |= 0B00000101; //CS00=CS02=1
	TCCR0B &= 0B11111101; //CS01=0
	
	//FAST PWM MODE TOP OCR0A
	TCCR0B &= 0B11110101; //WGM02=WGM01=0
	TCCR0A |= 0B00000001; //WGM00=1

	//ENABLE SPECIFIC INTERRUPTS (ALL)
	TIMSK0 |= 0B00000111;
	
	//MODE
	TCCR0A |= 0B10100000; //COM0A0=COM0B0=1
	
	OCR0A = 170;
	OCR0B = 100;
	
	//set pins to output
	DDRD   |= 0B01100000;//OC0A,OC0B
	
	
}

void main_function()//this goes in while(1) as a loop, repeats itself
{
	swn(inter,10,1);
	_delay_ms(1000);
		
}




ISR  (TIMER0_OVF_vect){
	//inter++;
	//sw("o\n\r");
}

ISR  (TIMER0_COMPA_vect){
	//sw("a\n\r");
}

ISR  (TIMER0_COMPB_vect){
	//sw("b\n\r");
}



void sw(char toWrite[]){
	len = strlen(toWrite); //take size of characters to be printed
	k=0;	//initialize i
	UDR0=0; //make sure UDR0 is 0
	
	while (k<len) //while i is less than the total length of the characters to be printed
	{
		if (  ((UCSR0A & 0B00100000) == 0B00100000)  ){ //if UDRE0 is 1 (aka UDR0 is ready to send)
			UDR0 = toWrite[k]; //put the next character to be sent (now, UDRE0 is 0)
			k++;		//increase the position, and wait until UDRE0 is 1 again
		}
	}
	udr0 = '\0';
}

void swn(int num, int type, int ln) //take an int and print it on serial
{
	char str_intx[50];//declare a string of 50 characters
	
	itoa(num, str_intx, type);//convert from int to char and save it on str
	
	sw(str_intx); //serial write str
	
	if(ln == 1) //if we want a new line,
	{
		sw("\n\r");
	}
	
}

/*
Input: float number, output: print float number to serial, OR only return the float as string.
*/
void swf(double numf, uint8_t lnf, uint8_t extra_accur, uint8_t print_it) //Take a float and print it on serial
{

	if(extra_accur == 1)
	{
		sprintf(str_floatx,"%d.%02u", (int) numf, (int) fabs(((numf - (int) numf ) * 1000)));
	}
	else
	{
		sprintf(str_floatx,"%d.%02u", (int) numf, (int) fabs(((numf - (int) numf ) * 100))); //edited to accept negative numbers
	}

	//sprintf(str_floatx,"%d.%02u", (int) numf, (int) ((numf - (int) numf ) * 100) ); //this was te original function

	if(print_it)
	{
		sw(str_floatx);	
		
		if(lnf == 1) //if we want a new line,
		{
			sw("\n\r");
		}
	}
}

//==================================TX END=====================================



void readUntill(char c_rx)
{
	rxWritePos = 0;//begin writing in the buffer from pos 0
	readString = 1;//interrupt will read and save strings
	rx_stop = c_rx; //interrupt will use the global var rx_stop to stop reading the string
	
	do{
		
	}while(readString == 1);
}

ISR(USART_RX_vect)
{
	if(readString == 1)
	{	
		rxBuffer[rxWritePos] = UDR0;
		if(rxBuffer[rxWritePos] == rx_stop)
		{
			readString = 0;
			/*when you initialize a character array by listing all of its characters 
			separately then you must supply the '\0' character explicitly */
			rxBuffer[rxWritePos] = '\0' ;
		}
		rxWritePos++;
	}
	else
	{
		udr0 = UDR0;	
	}
}

//==================================RX END==================================
